<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Exercice 38</title>
    </head>
    <body>
        <?php
        
            function select_jour($nomJour, $jourDefault = 1){
                echo "<select name=\"".$nomJour."\"><br>\n\t";
                    for($i = 1; $i <= 31; $i++){
                        if($i != $jourDefault){
                            echo "<option value=\"".$i."\">$i</option>\n\t";
                        }else{
                            echo "<option value=\"".$i."\" selected>$i</option>\n\t";
                        }   
                    }
                echo "</select><br>\n\t";
            }
            function select_mois($nomMois, $moisDefault = 1){
                $tabMois = array(1=>"Janvier","Février","Mars","Avril","Mai","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre");
                echo "<select name=\"".$nomMois."\"><br>\n\t";
                    for($i = 1; $i <= 12; $i++){
                        if($tabMois[$i] != $moisDefault){
                            echo "<option value=\"".$tabMois[$i]."\">$tabMois[$i]</option>\n\t";
                        }else{
                            echo "<option value=\"".$tabMois[$i]."\" selected>$tabMois[$i]</option>\n\t";
                        }   
                    }
                echo "</select><br>\n\t";
            }
            function select_heure($nomHeure, $heureDefault = 1){
                echo "<select name=\"".$nomHeure."\"><br>\n\t";
                    for($i = 0; $i <= 23; $i++){
                        if($i != $heureDefault){
                            echo "<option value=\"".$i."\">$i</option>\n\t";
                        }else{
                            echo "<option value=\"".$i."\" selected>$i</option>\n\t";
                        }   
                    }
                echo "</select><br>\n\t";
            }
            function select_minute($nomMinute, $minuteDefault = 1){
                echo "<select name=\"".$nomMinute."\"><br>\n\t";
                    for($i = 1; $i <= 59; $i++){
                        if($i != $minuteDefault){
                            echo "<option value=\"".$i."\">$i</option>\n\t";
                        }else{
                            echo "<option value=\"".$i."\" selected>$i</option\n\t>";
                        }   
                    }
                echo "</select><br>\n\t";
            }
            function select_seconde($name, $default = null){
                echo select($name, 1, 59, $default);
            }
            
            
            function select($name,$db,$fin,$default){
                
                echo "<select name=\"".$name."\"><br>\n\t";
                 echo "<option value=\"null\">-</option>\n\t";
                    for($i = $db; $i <= $fin; $i++){
                        if($i != $default){
                            echo "<option value=\"".$i."\">$i</option>\n\t";
                        }else{
                            echo "<option value=\"".$i."\" selected>$i</option>\n\t";
                        }   
                    }
                echo "</select><br>\n\t";
            }
            
            
            echo "Jour : ";
            echo select_jour(1, 14);
            
            echo "Mois : ";
            echo select_mois(2, "Février");
            
            echo "Heure : ";
            echo select_heure(3, 12);
            
            echo "Minute : ";
            echo select_minute(4, 50);
            
            echo "Seconde : ";
            echo select_seconde(5, 40);
            
        ?>
    </body>
</html>

