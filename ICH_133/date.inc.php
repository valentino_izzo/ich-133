<?php
        
        function mois_courant(){
           $mois = date('n');
           echo mois_fr($mois);
        }
        
          function mois_fr($mois = null){
                      
          if($mois === null){ //=== fait donnée identique et type identique
             return mois_courant();
          }
          
          if(($mois < 1 or $mois > 12) or $mois == " "){
              echo "Votre argument n'est pas compris entre 1 et 12";
              exit;
          }
            
          $tab = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
          return $tab[$mois-1];
        }
        
        
        
?>

