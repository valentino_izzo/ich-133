<!DOCTYPE HTML>
<html lang = "fr"> 
<head>                                         
<title> Exercice 18 </title>                    
<meta charset="UTF-8">                          
</head>                                       
<body>
<?php
//print_r($_POST);
/*** Tableau des notes ***/
$tab_notes = Array ("Veuillez choisir","Insuffisant","Suffisant","Bien","Très Bien");

$nom = "";
$note = 0;
$email = "";
$message = "";

/*** Flag affichage form ***/
$view_form = 1;

/*** Test si le formulaire à été envoyé ***/
if(isset($_POST['send'])){
    
    $tab_error = Array();
    
    /*** Test les différents champs ***/
    if($_POST['nom'] == ""){
        $tab_error[] = "Le nom est obligatoire !";
    }else{
        $nom = $_POST['nom'];
    }
    if($_POST['email'] == ""){
        $tab_error[] = "L'E-mail est obligatoire !";
    }else{
        $email = $_POST['email'];
    }
    if($_POST['note'] == 0){
        $tab_error[] = "Veuillez choisir une note !";
    }else{
        $note = $_POST['note'];
    }
    
    if($_POST['message'] == ""){
        $tab_error[] = "Merci de laisser un commentaire !";
   
    }else{
        $message = $_POST['message'];
    }
    
    /*** Test si des données manquantes ont été detectées ***/
    if(sizeof($tab_error)){
        // Affichage des message d'erreur
        echo "<pre>";
        print_r($tab_error);
        echo "</pre>";
    }else{
        // Affichage des données correctes
        echo "<pre>";
        print_r($_POST);
        echo "</pre>";
        // Indique de ne pas afficher le formulaire
        $view_form = 0;
    }
}
    
if($view_form){
    

    echo "\n<table>";
    echo "\n\t<tr>";
    echo "<form method=\"post\" action=\"".$_SERVER['PHP_SELF']."\">";
    /*** Nom ***/
    echo "\n\t\t<td><label>Nom : </label></td>";
    echo "\n\t\t<td><input type=\"text\" name=\"nom\" value=\"".$nom."\"></td>";
    echo "\n\t</tr>";

    /*** Email ***/
    echo "\n\t<tr>";
    echo "\n\t\t<td><label>E-mail : </label></td>";
    echo "\n\t\t<td><input type=\"text\" name=\"email\" value=\"".$email."\"></td>";
    echo "\n\t</tr>";

    /*** Note ***/
    echo "\n\t<tr>";
    echo "\n\t\t<td><label>Note : </label></td>";
    echo "\n\t\t<td>";
    //Solution avec select
    echo "\n\t\t\t<select name=\"note\">";
    foreach($tab_notes AS $key => $text_note){
        echo "\n\t\t\t\t<option ";
        if($key == $note){
            echo " selected ";
        }
        echo "value=\"".$key."\">".$text_note."</option>";
    }
    echo "\n\t\t\t</select>";
    // Solution avec Radio
   /* foreach($tab_notes AS $key => $note){
        echo "\n\t\t\t<input type=\"radio\" name=\"note\" value=\"".$key."\">".$note."<br>";
    }*/

    echo "\n\t\t</td>";
    echo "\n\t</tr>";

    /*** Message ***/
    echo "\n\t<tr>";
    echo "\n\t\t<td><label>Message : </label></td>";
    echo "\n\t\t<td><textarea name=\"message\">".$message."</textarea></td>";
    echo "\n\t</tr>";

    /*** Submit ***/
    echo "\n\t<tr>";
    echo "\n\t\t<td></td>";
    echo "\n\t\t<td><input type=\"submit\" name=\"send\" value=\"Envoyer\"></td>";
    echo "\n\t</tr>";

    echo "</form>";

    echo "</table>";
}
?>
</body>
</html>
