<html>
    <head>
        <meta charset="UTF-8">
        <title>Exercice 5</title>
    </head>
    <body>
    <?php
        define("pie", 3.14);
        $rayon = 2;
        $circonference = $rayon * 2 * pie;
        $surface = $rayon * $rayon * pie;
        
        echo "Le diamètre est ".$rayon."<br>";
        echo "La circonférence est ".$circonference."<br>";
        echo "La surface est ".$surface."<br>";
    ?>
    </body>
</html>



