<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Exercice 18</title>
    </head>
    <body>
        <?php
        echo "<pre>";
        print_r($_POST);
        echo "</pre>";
        $notes = Array (1=>"Insuffisant",2=>"Suffisant",3=>"Bien",4=>"Très bien");
        
        $error = false;
        
        $error_msg = "";
        
        // Vérifie que le nom est setté et qu'il n'est pas vide
        if(isset($_POST['nom']) && !empty($_POST['nom'])){
            $nom = $_POST['nom'];
        }else{
            $nom = "";
            $error = true;
            $error_msg .= "Veuillez saisir un nom<br>";
        }
       
        if(isset($_POST['email']) && !empty($_POST['email'])){
            $email = $_POST['email'];
        }else{
            $email = "";
            $error = true;
            $error_msg .= "Veuillez saisir un email<br>";
        }
        
        if(isset($_POST['note']) && !empty($_POST['note'])){
            $note = $_POST['note'];
        }else{
            $note = "";
            $error = true;
            $error_msg .= "Veuillez saisir une note<br>";
        }
        
        if(isset($_POST['message']) && !empty($_POST['message'])){
            $message = $_POST['message'];
        }else{
            $message = "";
            $error = true;
            $error_msg .= "Veuillez saisir un message<br>";
        }
        
        // Si le bouton valider n'est pas connu cela signifie que le formulaire n'a pas été envoyé 
        // OU la varialbe $erreur est initialisée à true (identique à $errror == true)
        if(!isset($_POST['valider'])  || $error){
            
            // Affichage des messages d'erreur uniquement sur validation du formulaire
            if(isset($_POST['valider'])){
                echo $error_msg;
            }

            echo "<form method=\"post\" action=\"".$_SERVER['PHP_SELF']."\">";
            echo "\n\t<label>Nom</label>";
            echo "\n\t<input type=\"text\" name=\"nom\" value=\"".$nom."\"><br>";
            echo "\n\t<label>E-mail</label>";
            echo "\n\t<input type=\"text\" name=\"email\" value=\"".$email."\"><br>";
            echo "\n\t<label>Note</label>";
            foreach($notes AS $num => $texte){
                echo "\n\t<input type=\"radio\" name=\"note\" value=\"".$num."\" ";
                if($note == $num){
                    echo " checked ";
                }
                echo ">".$texte."<br>";
            }
            echo "\n\t<label>Message</label>";
            echo "\n\t<textarea name=\"message\">".$message."</textarea><br>";
            echo "\n\t<input type=\"submit\" name=\"valider\" value=\"Envoyer\"><br>";

            echo "\n</form>";
        }else{
            
            
            
            echo "\nnom : ".$_POST['nom']."<br>";
            echo "\nemail : ".$_POST['email']."<br>";
            echo "\nnote : ".$notes[$_POST['note']]."<br>";
            echo "\nmessage : ".$_POST['message']."<br>";
            
        }
        
        
        
        ?>
    </body>
</html>

